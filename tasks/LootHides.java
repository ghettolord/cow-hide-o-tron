package cowhider.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.BasicQuery;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GroundItem;

import cowhider.CowHider;

public class LootHides extends Task<ClientContext> {
	
	private CowHider ch = null;
	private int inventoryCount = 0;
	private GroundItem cowhide = null;
	
	public LootHides(ClientContext ctx, CowHider ch) {
		super(ctx);
		this.ch = ch;
	}

	@Override
	public boolean activate() {
		return ctx.inventory.select().count() < 28 // have inventory space
			&& !ctx.groundItems.select().id(CowHider.COWHIDE_ID).viewable().isEmpty(); // & there's cowhides (in vision)
	}

	@Override
	public void execute() {
		System.out.println("looting..");
		
		// if more than 30 energy, enable run
		if (ctx.movement.energyLevel() > 30) {
			ctx.movement.running(true);
		}
		
		final BasicQuery<GroundItem> cowhides = ctx.groundItems.select().id(CowHider.COWHIDE_ID).nearest();
		
		for (GroundItem item : cowhides) {
			if (CowHider.COW_AREA.contains(item)) {
				cowhide = item;
				break;
			}
		}
		
		if(cowhide != null) {
			inventoryCount = ctx.inventory.select().count();
			if (cowhide.inViewport()) {
				cowhide.interact("Take", "Cowhide");
				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() {
						// continue once item has been picked up
						return inventoryCount < ctx.inventory.select().count();
					}
				}, 200, 10);
			} else {
				ctx.movement.step(cowhide);
				ctx.camera.turnTo(cowhide);

				Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() {
						// continue once item is in viewport
						return cowhide.inViewport();
					}
				}, 200, 5);
				cowhide.interact("Take", "Cowhide");
			}
			System.out.println(ctx.inventory.select().count());
			System.out.println(inventoryCount);
			if (ctx.inventory.select().count() > inventoryCount) {
				ch.collectedCowHide();
			}
		}
	}

}
