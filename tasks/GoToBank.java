package cowhider.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.TilePath;

import cowhider.CowHider;

public class GoToBank extends Task<ClientContext> {
	private TilePath pathToStairs = ctx.movement.newTilePath(CowHider.TILES_TO_STAIRS);
	Tile nextTile = null;
	
	public GoToBank(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		return ctx.inventory.select().count() == 28;
	}

	@Override
	public void execute() {
		System.out.println("going to bank..");
		
		if (CowHider.TOP_AREA.contains(ctx.players.local())) {
			// if we are at top floor, move to booth
			ctx.movement.step(CowHider.BOOTH);
			
			Condition.wait(new Callable<Boolean>() {
				@Override
				public Boolean call() {
					// once gate open, continue
					return ctx.movement.distance(CowHider.BOOTH) < 3;
				}
			}, 100, 10);
		} else if (CowHider.MIDDLE_STAIR_AREA.contains(ctx.players.local())) {
			// if we are at middle floor at stairs, move up
			handleObstacles();
		} else if (CowHider.LOWER_STAIR_AREA.contains(ctx.players.local())) {
			// if we are at lower floor at stairs, move up
			handleObstacles();
		} else {
			// else move to lower stairs
			nextTile = pathToStairs.next();
			
			// if next tile isnt reachable, handle obstacles
			if (nextTile != null) {
				if (!ctx.movement.reachable(ctx.players.local().tile(), nextTile)) {
					handleObstacles();
				} else {
					// next tile is reachable, gogo
					Tile playerLocation = ctx.players.local().tile();
					pathToStairs.traverse();
					
					Condition.wait(new Callable<Boolean>() {
						@Override
						public Boolean call() {
							// once gate open, continue
							return ctx.movement.distance(nextTile) < 8;
						}
					}, 200, 10);
					
					if (playerLocation == ctx.players.local().tile()) {
						// stuck.. 
						handleObstacles();
					}
				}
			} else {
				ctx.movement.step(CowHider.LOWER_STAIR_AREA.getRandomTile());
			}
		}
	}
	
	private void handleObstacles() {
		if (ctx.objects.select().id(16671).nearest().poll().inViewport()) {
			System.out.println("climb-up stairs");
			climbLowerStairs();
		} else if (ctx.objects.select().id(16672).nearest().poll().inViewport()) {
			System.out.println("climb-up stairs");
			climbMiddleStairs();
		} else if (ctx.objects.select().id(7158).nearest().poll().inViewport()) {
			System.out.println("open gate..");
			openGate();
		}
	}
	
	private void climbLowerStairs() {
		GameObject stairs = ctx.objects.select().id(16671).nearest().poll();	
		stairs.interact("Climb-up", "Staircase");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return CowHider.MIDDLE_STAIR_AREA.contains(ctx.players.local());
			}
		}, 200, 4);
	}
	
	private void climbMiddleStairs() {
		GameObject stairs = ctx.objects.select().id(16672).nearest().poll();
		stairs.interact("Climb-up", "Staircase");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return CowHider.TOP_AREA.contains(ctx.players.local());
			}
		}, 200, 4);
	}
	
	private void openGate() {
		GameObject gate = ctx.objects.select().id(7158).nearest().poll();
		gate.interact("Open", "Gate");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return !ctx.objects.select().id(7159).isEmpty();
			}
		}, 200, 10);
	}
}
