package cowhider.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;

import cowhider.CowHider;

public class Deposit extends Task<ClientContext> {
	public Deposit(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// Deposit items, if currently at bank and have items in inventory
		return ctx.objects.select().id(18491).poll().inViewport() && ctx.inventory.select().count() > 0;
	}

	@Override
	public void execute() {
		System.out.println("depositing..");
		if(ctx.bank.opened()) {
			ctx.bank.depositInventory();
		} else {
			// open bank
			GameObject booth = ctx.objects.select().id(18491).nearest().poll();
			booth.interact("Bank", "Bank booth");
			
			Condition.wait(new Callable<Boolean>() {
				@Override
				public Boolean call() {
					// continue once item is in viewport
					return ctx.bank.opened();
				}
			}, 200, 10);	
			if (!ctx.bank.opened()) {
				ctx.movement.step(CowHider.BOOTH);
			}
		}
	}
	

}