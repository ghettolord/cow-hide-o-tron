package cowhider.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Area;
import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;
import org.powerbot.script.rt4.TilePath;

import cowhider.CowHider;

public class GoToCows extends Task<ClientContext> {
	private TilePath pathToCows = ctx.movement.newTilePath(CowHider.TILES_TO_STAIRS).reverse();
	Tile nextTile = null;
	
	Area outerArea = new Area(new Tile(3239, 3299, 0), new Tile(3265, 3255, 0));
	
	public GoToCows(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		return !CowHider.COW_AREA.contains(ctx.players.local());
	}

	@Override
	public void execute() {
		System.out.println("going to cows..");
		
		if (CowHider.TOP_AREA.contains(ctx.players.local())) {
			// if we are at top floor, climb-down
			ctx.movement.step(CowHider.TOP_STAIRS);
			
			Condition.wait(new Callable<Boolean>() {
				@Override
				public Boolean call() {
					// go on once stairs are visible
					return ctx.objects.select().within(CowHider.TOP_AREA).id(16673).poll().inViewport();
				}
			}, 100, 10);
			handleObstacles();
		} else if (CowHider.MIDDLE_STAIR_AREA.contains(ctx.players.local())) {
			// if we are at middle floor, climb-down
			handleObstacles();
		} else {
			// move to cows !!!
			nextTile = pathToCows.next();
			Tile playerLocation = ctx.players.local().tile();
			
			// if next tile isnt reachable, handle obstacles
			if (nextTile != null) {
				if (!ctx.movement.reachable(ctx.players.local().tile(), nextTile)) {
					handleObstacles();
				} else {
					pathToCows.traverse();
					
					Condition.wait(new Callable<Boolean>() {
						@Override
						public Boolean call() {
							// once gate open, continue
							return ctx.movement.distance(nextTile) < 8;
						}
					}, 200, 10);
				}
			}
					
			if (ctx.players.local().tile().equals(playerLocation)) {
				// didn't move, might be stuck
				handleObstacles();
				pathToCows.traverse();
				ctx.movement.step(CowHider.COW_AREA.getRandomTile());
				if(outerArea.contains(ctx.players.local())) {
					ctx.movement.step(new Tile(3254, 3266, 0));
					
					Condition.wait(new Callable<Boolean>() {
						@Override
						public Boolean call() {
							// once in area, continue
							return CowHider.COW_AREA.contains(ctx.players.local());
						}
					}, 200, 10);
				}
			}
		}
	}
	
	private void handleObstacles() {
		if (ctx.objects.select().within(CowHider.TOP_AREA).id(16673).poll().inViewport()) {
			System.out.println("climb-down-stairs..");
			climbUpperStairs();
		} else if (ctx.objects.select().id(16672).nearest().poll().inViewport()) {
			System.out.println("climb-down stairs..");
			climbMiddleStairs();
		} else if (ctx.objects.select().id(7158).nearest().poll().inViewport()) {
			System.out.println("open gate..");
			openGate();
		}
	}
	
	private void climbUpperStairs() {
		GameObject stairs = ctx.objects.select().id(16673).nearest().poll();	
		stairs.interact("Climb-down", "Staircase");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return CowHider.MIDDLE_STAIR_AREA.contains(ctx.players.local());
			}
		}, 200, 4);
	}
	
	private void climbMiddleStairs() {
		GameObject stairs = ctx.objects.select().id(16672).nearest().poll();
		stairs.interact("Climb-down", "Staircase");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return CowHider.LOWER_STAIR_AREA.contains(ctx.players.local());
			}
		}, 200, 4);
	
	}
	
	private void openGate() {
		System.out.println("trying to open gate");
		GameObject gate = ctx.objects.select().id(7158).nearest().poll();
		gate.interact("Open", "Gate");
		
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				// once gate open, continue
				return !ctx.objects.select().id(7159).isEmpty();
			}
		}, 100, 10);
	}
	

}
