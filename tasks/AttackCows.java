package cowhider.tasks;

import org.powerbot.script.rt4.Npc;

import cowhider.CowHider;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.BasicQuery;
import org.powerbot.script.rt4.ClientContext;

public class AttackCows extends Task<ClientContext> {
	private Npc currentCow = null;
	
	public AttackCows(ClientContext ctx) {
		super(ctx);
	}
	
	@Override
	public boolean activate() {
		return CowHider.COW_AREA.contains(ctx.players.local())
				&& (!ctx.players.local().inCombat() || !ctx.players.local().interacting().valid());
	}
	
	@Override
	public void execute() {
		System.out.println("attacking..");
		
		// if more than 30 energy, enable run
		if (ctx.movement.energyLevel() > 30) {
			ctx.movement.running(true);
		}

		BasicQuery<Npc> cows = ctx.npcs.select().within(CowHider.COW_AREA).id(CowHider.COW_IDS).nearest();
		for (Npc cow : cows) {
			this.currentCow = cow;
			
			if (!cow.inCombat() && (cow.animation() != 5850)) {
				if (cow.inViewport()) {
					cow.interact("Attack");
				} else {
					ctx.movement.step(currentCow);
					ctx.camera.turnTo(currentCow);
					
					Condition.wait(new Callable<Boolean>() {				
						@Override
						public Boolean call() {
							// continue once cow in viewport
							return currentCow.inViewport();
						}
					}, 200, 10);
					currentCow.interact("Attack");
				}
				
				Condition.sleep(1500);
				Condition.wait(new Callable<Boolean>() {				
					@Override
					public Boolean call() {
						// continue once in combat
						return ctx.players.local().inCombat() || !ctx.players.local().interacting().valid();
					}
				}, 200, 40);
				
				/*Condition.wait(new Callable<Boolean>() {
					@Override
					public Boolean call() {
						// continue once out of combat
						return !ctx.players.local().inCombat() && !ctx.players.local().inMotion();
					}
				}, 200, 20);*/
				break;
			}
		}
	}
}
