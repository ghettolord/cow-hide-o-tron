package cowhider.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.powerbot.script.rt4.ClientContext;

import cowhider.CowHider;

public class GUI extends JFrame {
	
	private ClientContext ctx = null;
	private CowHider ch = null;
	
	private JPanel titlePanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	
	private JLabel titleLabel = new JLabel("cow-hide-o-tron");
	
	private JButton lootButton = new JButton("Fight & Loot");
	private JButton fightButton = new JButton("Fight only");
	
	private Font font = new Font("Arial", Font.BOLD, 12);
	
	public GUI(ClientContext ctx, CowHider ch) {
		this.ctx = ctx;
		this.ch = ch;
		init();
	}
	
	private void init() {
		setLayout(new BorderLayout());
		
		titleLabel.setFont(font);
		
		lootButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// init with loot
				ch.init(true);
				dispose();
			}
		});
		
		fightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// init without loot
				ch.init(false);
				dispose();
			}
		});
		
		titlePanel.add(titleLabel);
		buttonPanel.add(lootButton);
		buttonPanel.add(fightButton);
		add(titlePanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.CENTER);
		
		setTitle("cow-hide-o-tron");
		setSize(300, 90);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				ctx.controller.stop();
				dispose();
			}
		});
		
	}
}
