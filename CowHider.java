package cowhider;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.powerbot.script.Area;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;

import cowhider.gui.GUI;
import cowhider.tasks.AttackCows;
import cowhider.tasks.Deposit;
import cowhider.tasks.GoToBank;
import cowhider.tasks.GoToCows;
import cowhider.tasks.LootHides;
import cowhider.tasks.Task;


@Script.Manifest(name ="cow-hide-o-tron", description = "hides cows..", properties = "game=4;")
public class CowHider extends PollingScript<ClientContext> implements PaintListener {
	// Constants
	public static final int COWHIDE_ID = 1739;
	public static final int[] COW_IDS = {2805, 2806, 2807, 2808, 2809};
	
	public static final Tile BOOTH = new Tile(3208, 3220, 2);
	public static final Tile TOP_STAIRS = new Tile(3205, 3209, 2);
	public static final Tile[] TILES_TO_STAIRS = {
		new Tile(3252, 3287, 0), // somewhere inside
		new Tile(3254, 3279, 0), // between
		new Tile(3254, 3266, 0), // inside cow area at gate
		new Tile(3250, 3266, 0), // outside cow area at gate
		new Tile(3246, 3265, 0), // between
		new Tile(3235, 3262, 0), // on bridge
		new Tile(3231, 3262, 0), // between
		new Tile(3227, 3261, 0), // between
		new Tile(3218, 3255, 0), // at furnace
		new Tile(3219, 3247, 0), // between
		new Tile(3223, 3239, 0), // at guide
		new Tile(3230, 3232, 0), // between
		new Tile(3233, 3226, 0), // between
		new Tile(3233, 3224, 0), // in front of castle
		new Tile(3225, 3219, 0), // between
		new Tile(3232, 3219, 0), // between
		new Tile(3223, 3219, 0), // between
		new Tile(3218, 3219, 0), // at castle entrance
		new Tile(3214, 3209, 0), // between
		new Tile(3206, 3209, 0)}; // at stairs
	
	public static final Area LOWER_STAIR_AREA = new Area(new Tile(3205, 3208, 0), new Tile(3208, 3211, 0));
	public static final Area MIDDLE_STAIR_AREA = new Area(new Tile(3200, 3200, 1), new Tile(3220, 3220, 1));
	public static final Area TOP_AREA = new Area(new Tile(3000, 3000, 2), new Tile(3300, 3220, 2));
	public static final Area COW_AREA = new Area(
		new Tile(3265, 3255, 0),
		new Tile(3265, 3296, 0),
		new Tile(3263, 3298, 0),
		new Tile(3263, 3298, 0),
		new Tile(3242, 3284, 0),
		new Tile(3248, 3278, 0),
		new Tile(3253, 3279, 0),
		new Tile(3253, 3255, 0));
	
	// Variables
	private String mode = "fight & loot";
	
	private boolean startTasks = false;
	
	private int  cowHideGePrice = 0;
	private int cowHideCollected = 0;
	private int startExp = 0;
	
	private List<Task> taskList = new ArrayList<Task>();
	
	// self
	
	@Override
	public void start() {
		GUI gui = new GUI(ctx, this);
		gui.setVisible(true);
		
		startExp = getExp();	
		// should get non hard coded price here..
		cowHideGePrice = 116;
		
	}
	
	@Override
	public void poll() {
		if (startTasks) {
			for (Task task : taskList) {
				if (task.activate()) {
					task.execute();
					break;
				}
			}
		}
	}
	
	public void init(boolean loot) {
		if (loot) {
			taskList.addAll(Arrays.asList(new Deposit(ctx), new GoToBank(ctx), new GoToCows(ctx), new LootHides(ctx, this), new AttackCows(ctx)));
		} else {
			taskList.addAll(Arrays.asList(new GoToCows(ctx), new AttackCows(ctx)));
			mode = "fight only";
		}
		startTasks = true;
	}
	
	public void collectedCowHide() {
		this.cowHideCollected++;
	}
	
	public int getExp() {
		final int[] experiences = ctx.skills.experiences();
		int sum = 0;
		for(int i : experiences)
			sum += i;
		return sum;
	}
	
	public static final Font ARIAL = new Font("Arial", Font.BOLD, 12);
	
	@Override
	public void repaint(Graphics graphics) {
		if (startTasks) {
			final Graphics2D g = (Graphics2D) graphics;
			g.setFont(ARIAL);
			
			final long currentRuntime = getRuntime();
			final long second = (currentRuntime / 1000) % 60;
			final long minute = (currentRuntime / (1000 * 60)) % 60;
			final long hour = (currentRuntime / (1000 * 60 * 60)) % 24;
			final String duration = String.format("Duration: %02d:%02d:%02d", hour, minute, second);
			
			final int expGained = getExp() - startExp;
			final int profit = cowHideGePrice * cowHideCollected;
			final int profitHr = (int) ((profit * 3600000D) / getRuntime());
			final int expHr = (int) ((expGained * 3600000D) / getRuntime());
			final int cowHidesHr = (int) ((cowHideCollected * 3600000D) / getRuntime());
			
			
			g.setColor(Color.BLACK);
			g.fillRect(5, 5, 200, 120);
			
			g.setColor(Color.PINK);
			g.drawString("cow-hide-o-tron", 10, 20);
			
			g.setColor(Color.CYAN);
			g.drawString("Mode: " + mode, 10, 40);
			
			g.setColor(Color.WHITE);
			g.drawString(duration, 10, 60);
			g.drawString(String.format("Hides: %,d (%,d/hr)", cowHideCollected, cowHidesHr), 10, 80);
			g.drawString(String.format("Profit: %,d (%,d/hr)", profit, profitHr), 10, 100);
			g.drawString(String.format("Exp: %,d (%,d/hr)", expGained, expHr), 10, 120);
		}
	}
}